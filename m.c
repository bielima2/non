#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "m.h"

void cg(M A, V b, V x, double eps) {
  fprintf(stdout, "Using the conjugate gradients method.\n");

  V r, sv, as, As;
  r.s = sv.s = as.s = As.s = x.s;

  r.v = (double *) malloc(sizeof(double) * x.s);
  sv.v = (double *) malloc(sizeof(double) * x.s);
  as.v = (double *) malloc(sizeof(double) * x.s);
  As.v = (double *) malloc(sizeof(double) * x.s);

  clock_t s;
  s = clock();

  vi(x, 0.0);
  mvm(&A, x, As);
  vs(b, As, r);
  vc(sv, r);

  long k = 0;
  while (k++ <= b.s) {
    mvm(&A, sv, As);
    double rr = dot(r, r);
    double a = rr / dot(sv, As);
    svm(sv, a, as);
    va(x, as, x);
    svm(As, a, As);
    vs(r, As, r);
    if (norm(r) <= eps) break;
    svm(sv, dot(r, r) / rr, sv);
    va(r, sv, sv);}

  double t = (double) (clock() - s) / CLOCKS_PER_SEC;
  fprintf(stdout, "Number of iterations: %ld\n", k);
  fprintf(stdout, "Time: %f seconds\n", t);

  free(r.v); free(sv.v); free(as.v); free(As.v);
}

void gd(M A, V b, V x, double eps) {
  fprintf(stdout, "Using the gradient descent method.\n");
  V r, Ar, ar;
  r.s = Ar.s = ar.s = x.s;

  r.v = (double *) malloc(sizeof(double) * x.s);
  Ar.v = (double *) malloc(sizeof(double) * x.s);
  ar.v = (double *) malloc(sizeof(double) * x.s);

  clock_t s;
  s = clock();

  vi(x, 0.0);
  mvm(&A, x, Ar);
  vs(b, Ar, r);

  unsigned long k = 0;
  while (k++ < N) {
    mvm(&A, r, Ar);
    double a = dot(r, r) / dot(r, Ar);
    svm(r, a, ar);
    va(x, ar, x);
    svm(Ar, a, Ar);
    vs(r, Ar, r);
    if (norm(r) <= eps) break;}

  double t = (double) (clock() - s) / CLOCKS_PER_SEC;
  fprintf(stdout, "Number of iterations: %ld\n", k);
  fprintf(stdout, "Time: %f seconds\n", t);

  free(r.v); free(Ar.v); free(ar.v);
}

void vi(V a, double v) {
  for (int i = 0; i < a.s; i++) {
    a.v[i] = v;}
}

void vc(V a, V b) {
  for (int i = 0; i < a.s; i++) {
    a.v[i] = b.v[i];}
}
void vs(V a, V b, V r) {
  for (int i = 0; i < a.s; i++) {
    r.v[i] = a.v[i] - b.v[i];}
}

void va(V a, V b,  V r) {
  for (int i = 0; i < a.s; i++) {
    r.v[i] = a.v[i] + b.v[i];}
}

void svm(V a, double x,  V r) {
  for (int i = 0; i < a.s; i++) {
    r.v[i] = a.v[i] * x;}
}

void mvm(M *A, V b,  V r) {
  if (A->m) {
    for (int i = 0; i < r.s; i++) {
      double t = 0;
      for (int j = 0; j < r.s; j++) {
        t += A->m[i][j] * b.v[j];}
      r.v[i] = t;}}
  else {
    int j = 0;
    double t = 0;
    for (long i = 0; i < A->n; i++) {
      t += A->v[i] * b.v[A->r[i]];
      if (A->c[i+1] > A->c[i]) {
        r.v[j] = t;
        t = 0;
        j++;}}
    r.v[j] = t;}
}

double norm(V a) {
  return sqrt(dot(a, a));
}

double dot(V a, V b) {
  double r = 0;

  for (int i = 0; i < a.s; i++) {
    r += a.v[i] * b.v[i];}

  return r;
}
