#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "h.h"
#include "m.h"

int main(int argc, char *argv[]) {
  Params p = getParams(argc, argv);
  if (p.e) return p.e;

  V x;
  x.s = p.v.s;
  x.v = (double *) malloc(sizeof(double) * p.v.s);

  if (!strcmp("gd", p.s)){
    gd(p.m, p.v, x, p.eps);}
  else if (!strcmp("cg", p.s)){
    cg(p.m, p.v, x, p.eps);}
  else {
    cg(p.m, p.v, x, p.eps);}

  FILE *o = fopen(p.o, "w");
  if (!o) {
    fprintf(stderr, "Could not open the output file, using stdout.\n");
    o = stdout;}

  V f;
  f.s = p.v.s;
  f.v = (double *) malloc(sizeof(double) * p.v.s);
  mvm(&p.m, x, f);
  for (int i = 0; i < p.v.s; i++) {
    if (f.v[i] < 1.0e-20 && f.v[i] > -1.0e-20) {
      f.v[i] = 0.0;}}
  for (int i = 0; i < p.v.s; i++) {
    fprintf(o, "s: %0.1e, ", f.v[i]);
    fprintf(o, "b: %0.1e, ", p.v.v[i]);
    fprintf(o, "x: %0.1e\n", x.v[i]);}
  free(f.v);
  fclose(o);
  free(x.v);
  d(&p);
  return 0;
}
