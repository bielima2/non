#ifndef H_H_
#define H_H_

#include "m.h"

typedef struct Params {
  int argc;
  char **argv;
  int e;
  char *m_path;
  char *v_path;
  M m;
  V v;
  double eps;
  char *s;
  char *o;
} Params;


Params getParams(int argc, char *argv[]);
void d(Params *p);
void loadData(Params *p);

#endif
