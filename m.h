#ifndef M_H_
#define M_H_

#define N 10000000

typedef struct M {
  double **m;

  double *v;
  int *c;
  int *r;
  long n;
} M;

typedef struct V {
  double *v;
  int s;
} V;

void gd(M A, V b, V r, double eps);
void cg(M A, V b, V r, double eps);
void vi(V a, double v);
void vc(V a, V b);
void vs(V a, V b, V r);
void va(V a, V b, V r);
void mvm(M* A, V b, V r);
void svm(V a, double x, V r);
double norm(V a);
double dot(V a, V b);

#endif
