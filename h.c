#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

#include "h.h"

Params getParams(int argc, char *argv[]) {
  Params p;
  p.m_path = NULL;
  p.v_path = NULL;
  p.e = 0;
  p.m.m = NULL;
  p.m.v = NULL;
  p.m.c = p.m.r = NULL;
  p.eps = 0.001;
  p.argc = argc;
  p.argv = argv;
  p.s = "cg";
  p.o = NULL;

  char *p_str = "m:v:e:s:o:";
  int c;

  //ARGUMENTS PROCESSING
  while ((c = getopt(argc, argv, p_str)) != -1)
    switch (c) {
    case 'm':
      p.m_path = optarg;
      break;
    case 'v':
      p.v_path = optarg;
      break;
    case 'e':
      p.eps = atof(optarg);
      break;
    case 's':
      p.s = optarg;
      break;
    case 'o':
      p.o = optarg;
      break;
      case '?':
        if (isprint(optopt))
          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
        p.e = 1;
    default:
      break;
    }

  if (!p.m_path || !p.v_path || p.e) {
    p.e = 1;
    fprintf(stderr, "No or wrong data specified, bye.\n");
    return p;
  }

  loadData(&p);
  return p;
}

void loadData(Params *p) {
  FILE *f = fopen(p->m_path, "r");
  if (!f) {
    fprintf(stderr, "Couldn't load the matrix, bye.\n");
    p->e = 1;
    return;
  }

  p->v.s = 0;
  fscanf(f, "%d", &p->v.s);

  if (!p->v.s) {
    fprintf(stderr, "Couldn't determine the vector size, bye.\n");
    p->e = 1;
    fclose(f);
    return;
  }

  char i = '\0';
  fscanf(f, "%c", &i);
  if (i == '\n') {
    //read the full matrix
    p->m.m = (double**) malloc(sizeof(double *) * p->v.s);

    for (int i = 0; i < p->v.s; i++) {
      p->m.m[i] = (double*) malloc(sizeof(double) * p->v.s);
      for (int j = 0; j < p->v.s; j++) {
        fscanf(f, "%lf", &(p->m.m[i][j]));
      }
    }
  } else {
    //read the compressed matrix
    p->m.n = 0;
    fscanf(f, "%ld", &p->m.n);
    if (!p->m.n) {
      fprintf(stderr, "Couldn't determine the matrix size, bye.\n");
      p->e = 1;
      fclose(f);
      return;
    }

    p->m.c = (int *) malloc(sizeof(int) * (p->m.n + 1));
    p->m.r = (int *) malloc(sizeof(int) * p->m.n);
    p->m.v = (double *) malloc(sizeof(double) * p->m.n);

    fscanf(f, "%d", p->m.c);
    fscanf(f, "%d", p->m.r);
    fscanf(f, "%lf", p->m.v);

    int o = 0;
    if (p->m.c[0]) o = 1;
    p->m.c[0] = p->m.c[0] - o;
    p->m.r[0] = p->m.r[0] - o;

    for (int i = 1; i < p->m.n; i++) {
      fscanf(f, "%d", &(p->m.c[i]));
      p->m.c[i] = p->m.c[i] - o;
      fscanf(f, "%d", &(p->m.r[i]));
      p->m.r[i] = p->m.r[i] - o;
      fscanf(f, "%lf", &(p->m.v[i]));
    }

    p->m.c[p->m.n] = -1;
  }

  fclose(f);
  f = NULL;
  f = fopen(p->v_path, "r");
  if (!f) {
    fprintf(stderr, "Couldn't load the vector, bye.\n");
    p->e = 1;
    d(p);
    return;
  }

  p->v.v = (double*) malloc(sizeof(double) * p->v.s);

  for (int i = 0; i < p->v.s; i++) {
    fscanf(f, "%lf", &(p->v.v[i]));
  }

  fclose(f);
}

void d(Params *p) {
  if (p->v.v) free(p->v.v);
  if (p->m.m) {
    for (int j = 0; j < p->v.s; j++) {
      if (p->m.m[j]) free(p->m.m[j]);
    }
    free(p->m.m);
  }
  if (p->m.v) free(p->m.v);
  if (p->m.r) free(p->m.r);
  if (p->m.c) free(p->m.c);
}
